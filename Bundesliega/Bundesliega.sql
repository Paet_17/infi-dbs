-- CREATE DATABASE bundesliega;
USE bundesliega;

-- Table Schiedsrichter
CREATE TABLE IF NOT EXISTS Schiedsrichter(
zulassungsnummer INT PRIMARY KEY,
vorname TEXT NOT NULL,
nachname TEXT NOT NULL);

CREATE TABLE IF NOT EXISTS Stadion(
name TEXT PRIMARY KEY,
ort TEXT NOT NULL,
plz TEXT NOT NULL,
straße TEXT NOT NULL,
hnr TEXT NOT NULL,
plätze INT NOT NULL);

CREATE TABLE IF NOT EXISTS Spieler(
spielerPassNr INT PRIMARY KEY
vorname TEXT NOT NULL,
nachname TEXT NOT NULL,
gebDatum DATE NOT NULL);

CREATE TABLE IF NOT EXISTS Team(
Vereinsname TEXT PRIMARY KEY
ort TEXT NOT NULL,
plz TEXT NOT NULL,
straße TEXT NOT NULL,
hnr TEXT NOT NULL);

CREATE TABLE IF NOT EXISTS Spiel(
spielID INT PRIMARY KEY,
heimMannschaft TEXT NOT NULL REFERENCES Team(vereinsname),
gästeMannschaft TEXT NOT NULL REFERENCES Team(vereinsname),
toreHeimMannschaft INT NOT NULL,
toreGästeMannschaft INT NOT NULL,
schiedsrichter INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
linienrichter1 INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
linienrichter2 INT NOT NULL REFERENCES Schiedsrichter(zulassungsNummer),
stadion TEXT NOT NULL REFERENCES Stadion(name),
datum DATE NOT NULL);


CREATE TABLE IF NOT EXISTS SpieltFür(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
vereinsname TEXT NOT NULL REFERENCES Team(vereinsname),
von DATE NOT NULL,
bis DATE NOT NULL,
PRIMARY KEY(spielerPassNr, vereinsname));

CREATE TABLE IF NOT EXISTS Karten(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
spielID INT NOT NULL REFERENCES Spiel(spielID),
zeitpunkt DATE NOT NULL,
farbe TEXT NOT NULL,
PRIMARY KEY(spielerPassNr, spielID));

CREATE TABLE IF NOT EXISTS Tor(
spielerPassNr INT NOT NULL REFERENCES Spieler(spielerPassNr),
spielID INT NOT NULL REFERENCES Spiel(spielID),
zeitpunkt DATE NOT NULL,
PRIMARY KEY(spielerPassNr, spielID));