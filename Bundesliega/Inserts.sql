USE bundesliega;

INSERT INTO Schiedsrichter VALUES(0, "Markus", "Berger");
INSERT INTO Schiedsrichter VALUES(1, "Tugay", "Bomberman");
INSERT INTO Schiedsrichter VALUES(2, "Ali", "Dag");
INSERT INTO Schiedsrichter VALUES(3, "Johannes", "Nichtschulmann");
INSERT INTO Schiedsrichter VALUES(4, "Bill", "Gains");

INSERT INTO Stadion VALUES("Lago-Arena", "Gardasee", "15296", "Lagostrasse", "100f", 10000);
INSERT INTO Stadion VALUES("Ring of Fire", "Bermingham", "4937", "Smithroad", "56c", 40000);
INSERT INTO Stadion VALUES("Gladiatorium", "Pitztol", "4296", "Bloodpick", "0", 300);
INSERT INTO Stadion VALUES("Nassareit-Stadion", "Nassareit", "1717", "Nerdroad", "10", 1700);
INSERT INTO Stadion VALUES("Anichstrasse-Anstalt", "Innsbruck", "6020", "Anichstrasse", "26", 3000);


INSERT INTO Spieler VALUES(0, "David", "Ruets da Silva", "1999-10-10");
INSERT INTO Spieler VALUES(1, "Luigi", "Marta Luna", "1999-5-30");
INSERT INTO Spieler VALUES(2, "Murat", "BMW", "1920-1-10");
INSERT INTO Spieler VALUES(3, "Andre", "Lauchi", "1998-4-5");
INSERT INTO Spieler VALUES(4, "Brot", "Pitt", "1999-9-10");
