import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;


@SuppressWarnings("serial")
public class GUI implements Serializable{

	private JFrame frame;
	private JTextField tBPName;
	private JTextField tBAname;
	private JTextField tBTgeplant;
	private JTextField tBTReal;
	JButton btnLoad;
	JLabel lblDiff;
	
	JFileChooser fc = new JFileChooser();
	
	
	JComboBox<Aufgabe> cBAufgabe;
	JComboBox<Projekt> cBProjekt;
	JTextArea taErklaehrung;
	Projekt executer = new Projekt("Executer");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Projektorganisator");
		frame.setResizable(false);
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		
		//Komponents
		JLabel lblProjekt = new JLabel("Projekt:");
		JButton btnPadd = new JButton("Projekt hinzufuegen");
		tBPName = new JTextField();
		cBProjekt = new JComboBox<Projekt>();
		JLabel lblAufgabe = new JLabel("Aufgabe:");
		cBAufgabe = new JComboBox<Aufgabe>();
		JButton btnAadd = new JButton("Aufgabe hinzufuegen");
		tBAname = new JTextField();
		JLabel lblZeitGeplant = new JLabel("Zeit geplant:");
		tBTgeplant = new JTextField();
		tBTReal = new JTextField();
		taErklaehrung = new JTextArea();
		JLabel lblDifferenz = new JLabel("Differenz:");
		lblDiff = new JLabel("000");
		 btnLoad = new JButton("Laden");
		
		
		lblProjekt.setBounds(10, 11, 67, 14);
		panel.add(lblProjekt);
		
		
		
		
		btnPadd.setEnabled(false);
		btnPadd.setBounds(428, 7, 150, 23);
		panel.add(btnPadd);
		btnPadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pAdd();
			}
		});
		
		
		tBPName.setBounds(267, 8, 151, 20);
		panel.add(tBPName);
		tBPName.setColumns(10);
		tBPName.addCaretListener(new CaretListener(){
			public void caretUpdate(CaretEvent arg0) {
				String s = tBPName.getText();
				s = s.trim();
				if(s.isEmpty())
				{
					btnPadd.setEnabled(false);
				}
				else
				{
					btnPadd.setEnabled(true);
				}
			}
		});
		
		
		cBProjekt.setBounds(111, 8, 146, 20);
		panel.add(cBProjekt);
		for(Projekt p : Main.pL)
		{
			cBProjekt.addItem(p);
		}
		cBProjekt.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					int i = cBProjekt.getSelectedIndex();
					tBPName.setText(Main.pL.get(i).toString());
					
					cBAufgabe.removeAllItems();
					
					for(Aufgabe a : Main.pL.get(i).aufgabenListe)
					{
						cBAufgabe.addItem(a);
					}
					
				}else if(e.getStateChange() == ItemEvent.DESELECTED)
				{
					
				}
			}
		});
		
		lblAufgabe.setBounds(10, 36, 77, 14);
		panel.add(lblAufgabe);
		
		
		cBAufgabe.setBounds(111, 39, 146, 20);
		panel.add(cBAufgabe);
		cBAufgabe.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					int i = cBAufgabe.getSelectedIndex();
					int ip = cBProjekt.getSelectedIndex();
					
					Aufgabe a = cBAufgabe.getItemAt(i);
					
					tBAname.setText(a.getAufgabeName());
					tBTgeplant.setText(String.valueOf(a.getZeitGeplant()));
					tBTReal.setText(String.valueOf(a.getZeitReal()));
					taErklaehrung.setText(a.getAufgabeErklaerung());
					lblDiff.setText(String.valueOf(a.getZeitGeplant()-a.getZeitReal()));
				}
			}
		});
		
		
		
		
		btnAadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = cBProjekt.getSelectedIndex();
				int ia = cBAufgabe.getComponentCount();
				aAdd(i, ia);
			}
		});
		btnAadd.setEnabled(false);
		btnAadd.setBounds(10, 300, 160, 23);
		panel.add(btnAadd);
		
		
		tBAname.setBounds(267, 39, 151, 20);
		panel.add(tBAname);
		tBAname.setColumns(10);
		tBAname.addCaretListener(new CaretListener(){
			public void caretUpdate(CaretEvent arg0) {
				String s = tBAname.getText();
				s = s.trim();
				if(s.isEmpty())
				{
					btnAadd.setEnabled(false);
				}
				else if((cBProjekt.getItemCount()) != 0)
				{
					btnAadd.setEnabled(true);
				}
			}
		});
		
		
		lblZeitGeplant.setBounds(10, 90, 89, 14);
		panel.add(lblZeitGeplant);
		
		JLabel lblZeitTats = new JLabel("Zeit real:");
		lblZeitTats.setBounds(10, 115, 77, 14);
		panel.add(lblZeitTats);
		
		
		lblDifferenz.setBounds(10, 140, 67, 14);
		panel.add(lblDifferenz);
		
		JLabel lblNewLabel = new JLabel("Erklaehrung:");
		lblNewLabel.setBounds(10, 165, 89, 14);
		panel.add(lblNewLabel);
		
		
		tBTgeplant.setText("0000");
		tBTgeplant.setBounds(111, 87, 286, 20);
		panel.add(tBTgeplant);
		tBTgeplant.setColumns(10);
		
		
		tBTReal.setText("000");
		tBTReal.setBounds(111, 112, 286, 20);
		panel.add(tBTReal);
		tBTReal.setColumns(10);
		
		JButton btnSave = new JButton("Speichern");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				fc.showSaveDialog(null);
				String path = fc.getSelectedFile().getAbsolutePath();
				executer.save(path);
			}
		});
		btnSave.setBounds(10, 350, 101, 23);
		panel.add(btnSave);
		
		
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fc.showOpenDialog(null);
				String path = fc.getSelectedFile().getAbsolutePath();
				try {
					executer.load(path);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				cBProjekt.removeAllItems();
				
				for(Projekt p : Main.pL)
				{
					cBProjekt.addItem(p);
				}
				
			}
		});
		btnLoad.setBounds(10, 384, 101, 23);
		panel.add(btnLoad);
		
		
		lblDiff.setBounds(111, 140, 286, 14);
		panel.add(lblDiff);
		
		
		taErklaehrung.setText("...");
		taErklaehrung.setBounds(111, 160, 360, 130);
		taErklaehrung.setLineWrap(true);
		taErklaehrung.setWrapStyleWord(true);
		panel.add(taErklaehrung);
		
		JButton btnALoeschen = new JButton("Aufgabe loeschen");
		btnALoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = cBProjekt.getSelectedIndex();
				int ia = cBAufgabe.getSelectedIndex();
				aLoeschen(i, ia);
			}
		});
		btnALoeschen.setBounds(121, 350, 146, 23);
		panel.add(btnALoeschen);
		
		JButton btnPLoeschen = new JButton("Projekt loeschen");
		btnPLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int ix = cBProjekt.getSelectedIndex();
				pLoeschen(ix);
			}
		});
		btnPLoeschen.setBounds(277, 350, 151, 23);
		panel.add(btnPLoeschen);
		
		JButton btnABearbeiten = new JButton("Aufgabe bearbeiten");
		btnABearbeiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = cBProjekt.getSelectedIndex();
				int ia = cBAufgabe.getSelectedIndex();
				ArrayList<Aufgabe> aL = Main.pL.get(i).getAufgabe();
				Aufgabe a = aL.get(ia);
				a.setAufgabeName(tBAname.getText());
				a.setZeitGeplant(Integer.parseInt(tBTgeplant.getText()));
				a.setZeitReal(Integer.parseInt(tBTReal.getText()));
				a.setAufgabeErklaerung(taErklaehrung.getText());
				aLoeschen(i, ia);
				Main.pL.get(i).aufgabeHinzufuegen(a);
				cBAufgabe.addItem(a);
				cBAufgabe.setSelectedIndex(cBAufgabe.getItemCount()-1);
				
			}
		});
		btnABearbeiten.setBounds(121, 384, 146, 23);
		panel.add(btnABearbeiten);
		btnABearbeiten.setToolTipText("Bitte zuerst die gewuenschten Felder aendern\n und dann zum Speichern auf diesen Button klicken.");
		
		JButton btnPBearbeiten = new JButton("Projekt bearbeiten");
		btnPBearbeiten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = cBProjekt.getSelectedIndex();
				Projekt p = Main.pL.get(i);
				p.setpName(tBPName.getText());
				pLoeschen(i);
				Main.pL.add(p);
				cBProjekt.addItem(p);
				cBProjekt.setSelectedIndex(cBProjekt.getItemCount()-1);
			}
		});
		btnPBearbeiten.setBounds(277, 384, 151, 23);
		panel.add(btnPBearbeiten);
		
		
	}
	public void pLoeschen(int ix)
	{
		cBAufgabe.removeAllItems();
		cBProjekt.removeItemAt(ix);
		Main.pL.remove(ix);
		if ((ix-1) == -1) {
			reset();
		}
		if(cBProjekt.getItemCount() >= 2) cBProjekt.setSelectedIndex((ix-1));
		
	}
	
	public void aLoeschen(int i, int ia)
	{
		
		cBAufgabe.removeItemAt(ia);
		Main.pL.get(i).aufgabeLöschen(ia);
		if(cBAufgabe.getItemCount() >= 2) cBAufgabe.setSelectedIndex((cBAufgabe.getSelectedIndex()-1));
	}
	
	public void aAdd(int i, int ia)
	{
		int zeitg = Integer.parseInt(tBTgeplant.getText());
		int zeitr = Integer.parseInt(tBTReal.getText());
		String name = tBAname.getText();
		String erkl = taErklaehrung.getText();
		Aufgabe a = Main.pL.get(i).aufgabenErstellen(ia, zeitg, zeitr, name, erkl);
		cBAufgabe.addItem(a);
		
		if(cBAufgabe.getItemCount() >= 2) cBAufgabe.setSelectedIndex((cBAufgabe.getSelectedIndex()+1));
	}
	
	public void pAdd()
	{
		Projekt p = new Projekt(tBPName.getText());
		Main.pL.add(p);
		cBProjekt.addItem(p);
		if(cBProjekt.getItemCount() >= 2) cBProjekt.setSelectedIndex(cBProjekt.getItemCount()-1);
		reset();
		tBPName.setText(p.toString());
	}
	
	public void reset()
	{
		tBPName.setText("");
		tBAname.setText("");
		tBTgeplant.setText("");
		tBTReal.setText("");
		lblDiff.setText("0");
		taErklaehrung.setText("");
		cBAufgabe.removeAllItems();
	}
}
