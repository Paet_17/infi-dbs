import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

@SuppressWarnings("serial")
public class Projekt implements Serializable {
	transient Scanner sc = new Scanner(System.in);
	public ArrayList<Aufgabe> aufgabenListe = new ArrayList<Aufgabe>();
	ISave iS;
	private String pName;

	public Projekt(String name) {
		this.setpName(name);
	}

	public ArrayList<Aufgabe> getAufgabe() {
		return aufgabenListe;
	}

	public void aufgabenAnzeigen() {

		for (Aufgabe a : aufgabenListe) {
			//ausgabe

		}
	}

	public Aufgabe aufgabenErstellen(int id, int zeitGep, int zeitReal, String aufgName, String aufgErk) {
		Aufgabe a = new Aufgabe(id, zeitGep, zeitReal, aufgName, aufgErk);
		aufgabenListe.add(a);
		return a;
	}
	
	public void aufgabeHinzufuegen(Aufgabe a)
	{
		aufgabenListe.add(a);
	}

	public void addRealeZeit(String aufgName, int zeitReal) {
		for (Aufgabe a : aufgabenListe) {
			if (aufgName.equals(a.getAufgabeName())) {

				a.setZeitReal(zeitReal);

			}
		}
	}

	public void aufgabeUeberschreiben(int id, int zeitGeplant, int zeitReal, String aufgabeName,
			String aufgabeErklaerung) {
		for (Aufgabe a : aufgabenListe) {
			if (id == a.getId()) {
				if (zeitGeplant != -1) {
					a.setZeitGeplant(zeitGeplant);
				}
				if (zeitReal != -1) {
					a.setZeitReal(zeitReal);
				}
				if (!aufgabeName.equals("none"))
					a.setAufgabeName(aufgabeName);
			}
			if (!aufgabeErklaerung.equals("none")) {
				a.setAufgabeErklaerung(aufgabeErklaerung);
			}
		}
	}

	public void aufgabeLöschen(int id) {
		aufgabenListe.remove(id);
	}

	public void save(String path) {
		iS = new ISave(Main.pL, aufgabenListe);
		try {
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out;
			try {
				out = new ObjectOutputStream(fileOut);
				out.writeObject(iS);
				out.close();
				fileOut.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void load(String path) throws IOException, ClassNotFoundException {
		FileInputStream f_in = new FileInputStream(path);
		ObjectInputStream obj_in = new ObjectInputStream(f_in);
		
		ISave iS = (ISave) obj_in.readObject();
		this.iS = iS;
		aufgabenListe = (ArrayList<Aufgabe>) this.iS.getaL();
		Main.pL = (ArrayList<Projekt>) this.iS.getpL();

		obj_in.close();

	}

	public String toString() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	
}
