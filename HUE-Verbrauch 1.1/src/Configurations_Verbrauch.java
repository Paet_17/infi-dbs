import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class Configurations_Verbrauch {
	
	private String dbName = "verbrauch.db";
	private String tbName1 = "strom_verbrauch";
	private String tbName2 = "wasser_verbrauch";
	
	Properties prop = new Properties();
	OutputStream output = null;
	InputStream input = null;
	
	public Configurations_Verbrauch()
	{
		try {
			output = new FileOutputStream("config.properties");
			
			prop.setProperty("database", dbName);
			prop.setProperty("tab1", tbName1);
			prop.setProperty("tab2", tbName2);
			
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally{
			if (output != null){
				try{
					output.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	
	public String getDbName()
	{
		String dbName = "";
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			dbName = prop.getProperty("database");
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		
		return dbName;
		}
	
	public String getTbName1()
	{
		String tbName1 = "";
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			tbName1 = prop.getProperty("tab1");
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		
		return tbName1;
	}

	public String getTbName2()
	{
		String tbName2 = "";
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			tbName2 = prop.getProperty("tab2");
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		
		return tbName2;
	}
}

