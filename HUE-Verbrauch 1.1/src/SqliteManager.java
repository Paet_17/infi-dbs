import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;



public class SqliteManager {
	
	private String dbName;
	private String tbName;
	private String tb2Name;
	
	Configurations_Verbrauch cV = new Configurations_Verbrauch();
	
	Connection c = null;
	PreparedStatement ps = null;
	
	public SqliteManager()
	{
		this.dbName = cV.getDbName();
		this.tbName = cV.getTbName1();
		this.tb2Name = cV.getTbName2();
	}
	
	public void openConnection()
	{
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:C:\\SQLight\\verbrauch.db");
			c.setAutoCommit(false);
				
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
		}
		
	}
	
	public void closeConnection() throws SQLException
	{
		c.close();
	}
	
	
	public double interpolate(String tbName, Date date) throws SQLException
	{
		long timestamp = ((long)date.getTime()/1000);
		openConnection();
		String stimestamp = Long.toString(timestamp);
		double wertk;
		double wertg;
		long datek;
		long dateg;
		
		
		String stm = "select datum, value from " + tbName + " where datum < ? order by datum limit 1";
		
		ps = c.prepareStatement(stm);
		ps.setLong(1, timestamp);
		{
			int[] arr = readI2();
			datek = arr[0];
			wertk = arr[1];
		}
		
		
		stm = "select datum, value from " + tbName + " where datum > " + stimestamp + 
				" order by datum limit 1";
		
		ps = c.prepareStatement(stm);
		{
			int[] arr = readI2();
			dateg = arr[0];
			wertg = arr[1];
		}
		
		
		closeConnection();
		
		
		System.out.println(wertk);
		System.out.println(wertg);
		System.out.println(dateg);
		System.out.println(datek);
		
		double interpolated =wertk+((wertg-wertk)/(dateg-datek))*(timestamp-datek);
		
		return interpolated;
	}

	private int[] readI2() throws SQLException {
		
		ResultSet rs = ps.executeQuery();
		int[] arr = new int[2];
		while(rs.next())
		{
			arr[0] = rs.getInt(1);
			arr[1] = rs.getInt(2);
			
		}
		rs.close();
		ps.close();
		return arr;
	}

}
