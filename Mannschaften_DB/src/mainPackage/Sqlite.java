package mainPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Sqlite {
	
	Connection c = null;
	
	int manschID ;
	int spielID;
	int abteilID;
	
	
	Sqlite() throws SQLException
	{
		createDatabase();
		manschID = zaehleElemente("mannschaft");
		spielID = zaehleElemente("spieler");
		abteilID = zaehleElemente("abteilung");
		
	}
	
	public void OpenConnection() 
	{
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:Vereine.db");
			c.setAutoCommit(false);
			
			
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}
	}

	public void CloseConnection() throws SQLException
	{
		c.close();
	}

	public void NeueMannschaft(String name, String kapitaen, int abteilungID, String trainer) throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("INSERT INTO mannschaft (mannschafts_id, name, kapitaen, abteilungs_id, trainer)VALUES(?,?,?,?,?)");
	      ps.setInt(1,manschID);
	      ps.setString(2, name);
	      ps.setString(3, kapitaen);
	      ps.setInt(4, (int) abteilungID);
	      ps.setString(5, trainer);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      manschID++;
	      CloseConnection();
	      
	}
	
	public void NeuerSpieler(String name, String vname, String plz, String ort, String strasse, String gebD, int[] manIDs, String seit) throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("INSERT INTO spieler (mitgliedsnummer, name, vorname, plz, ort, strasse, geburtsdatum)VALUES(?,?,?,?,?,?,?)");
	      ps.setInt(1,spielID);
	      ps.setString(2, name);
	      ps.setString(3, vname);
	      ps.setString(4, plz);
	      ps.setString(5, ort);
	      ps.setString(6, strasse);
	      ps.setString(7, gebD);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      spielID++;
	      //TODO debuggen
	      
	       
	    //  for(int i= 0; i <= (manIDs.length - 1); i++)
	      //{
	      
	    	  int x = manIDs[i];
	    	  SpieltIn(x, seit);
	   //   }
	      
	      CloseConnection();
	}
	
	public void NeueAbteilung(String name, String leiter) throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("INSERT INTO abteilung (abteilungs_id, abteilungs_name, abteilungs_leiter)VALUES(?,?,?)");
	      ps.setInt(1,abteilID);
	      ps.setString(2, name);
	      ps.setString(3, leiter);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      abteilID++;
	      CloseConnection();
	}
	
	public void SpieltIn(int manID, String eintrittD) throws SQLException
	{
		PreparedStatement ps = c.prepareStatement("INSERT INTO spieltIn (mitgliedsnummer, mannschafts_ID, eintritsdatum)VALUES(?,?,?)");
	      ps.setInt(1,spielID);
	      ps.setInt(2, manID);
	      ps.setString(3, eintrittD);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	}

	public int zaehleElemente(String name) throws SQLException
	{
		OpenConnection();
		int anz = 0;
		
		String zaehlen = ("select count(*) as anz from " + name + ";");
		PreparedStatement ps = c.prepareStatement(zaehlen);
		ResultSet rs = ps.executeQuery();
		while(rs.next())
		{
			anz = rs.getInt(1);
		}
		rs.close();
		ps.close();
		CloseConnection();
		
		return anz;
		
		
	}

	public void ausgabeMannschaft() throws SQLException
	{
		OpenConnection();
		 PreparedStatement ps = c.prepareStatement("select * from mannschaft;");
		 ResultSet rs = ps.executeQuery();
		 
		 System.out.println("\n\nAlle Mannschaften:\n");
		 
		 while(rs.next())
		 {
			 System.out.println("Manschaft nr(ID): \t" + rs.getInt("mannschafts_id") );
			 System.out.println("Name: \t" + rs.getString("name"));
			 System.out.println("Kapitaen =\t" + rs.getString("kapitaen"));
			 System.out.println("Abteilung: \t" + rs.getString("abteilungs_id"));
			 System.out.println("Trainer: \t" + rs.getString("trainer"));
			 System.out.println("\n");
		 }
		 ps.close();
		 rs.close();
		 CloseConnection();
	}

	public void ausgabeSpieler() throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("select * from spieler");
		ResultSet rs = ps.executeQuery();
		
		System.out.println("\n\n\nAlle Spieler: \n");
		
		while(rs.next())
		{
			System.out.println("Mitgliedsnummer: " + rs.getInt("mitgliedsnummer"));
			System.out.println("Nachname: " + rs.getString("name"));
			System.out.println("Vorname: " + rs.getString("vorname"));
			System.out.println("PLZ: " + rs.getString("plz"));
			System.out.println("Ort: " + rs.getString("strasse"));
			System.out.println("Geburtsdatum: " + rs.getString("geburtsdatum"));
			System.out.println("\n");
		}
		ps.close();
		rs.close();
		CloseConnection();
	}
	
	public void ausgabeAbteilung() throws SQLException
	{
		OpenConnection();
		PreparedStatement ps = c.prepareStatement("select * from abteilung");
		ResultSet rs = ps.executeQuery();
		
		System.out.println("\n\n\nAlle Abtelungen: \n");
		
		while(rs.next())
		{
			System.out.println("Abteilungsnr (id): " + rs.getInt("abteilungs_id"));
			System.out.println("Name: " + rs.getString("abteilungs_name"));
			System.out.println("Leiter: " + rs.getString("abteilungs_leiter"));
			System.out.println("\n");
		}
		ps.close();
		rs.close();
		CloseConnection();
	}

	public boolean Loeschen(String tab, int zeile, String idName) throws SQLException
	{
		OpenConnection();
		String comm = "DELETE FROM " + tab + " WHERE " + idName +  "=" + zeile;
		PreparedStatement ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		CloseConnection();
		return true;
		
	}

	public void createDatabase() throws SQLException
	{
		OpenConnection();
		String comm = "create table if not exists abteilung("
				+ "abteilungs_id int primary key,"
				+ "abteilungs_name varchar(50),"
				+ "abteilungs_leiter varchar(50));";
		PreparedStatement ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists mannschaft("
				+ "mannschafts_id int primary key,"
				+ "name varchar(50) not null,"
				+ "kapitaen varchar(40) not null,"
				+ "abteilungs_id int,"
				+ "trainer varchar(50) not null,"
				+ "foreign key(abteilungs_id) references abteilung);";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists spieler("
				+ "mitgliedsnummer int primary key,"
				+ "name varchar(30) not null,"
				+ "vorname varchar(30) not null,"
				+ "plz varchar(6) not null,"
				+ "ort varchar(50) not null,"
				+ "strasse varchar(50) not null,"
				+ "geburtsdatum varchar(10));";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists spieltin("
				+ "mitgliedsnummer int not null,"
				+ "mannschafts_id int not null,"
				+ "eintritsdatum varchar(10),"
				+ "primary key(mitgliedsnummer, mannschafts_id))";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		
		c.commit();
		ps.close();
		CloseConnection();
		
	}

}


