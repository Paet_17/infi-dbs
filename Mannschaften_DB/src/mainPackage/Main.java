package mainPackage;

import java.io.File;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {

	//Hilfsvariablen
	static String wahl;
	static Scanner scan = new Scanner(System.in);
	
	static Sqlite sql;
	
			
	public static void main(String[] args) throws SQLException {
		
		sql = new Sqlite();
		boolean weiter = true;
		
		
		do {
			weiter = Menue();
		} while (weiter);
		System.out.println("\nAuf Wiedersehen!");
		

	}
	
	public static boolean Menue() throws SQLException
	{
		System.out.println("Willkommen in ihrer Datenbank zur Verwaltung von Mannschaften! \n");
		System.out.println("Bitte waehlen sie eine Aktion:");
		System.out.println("Mannschaft eintragen \t ... \t m");
		System.out.println("Spieler eintragen \t ... \t s");
		System.out.println("Abteilung eintragen \t ... \t a");
		System.out.println("Ausgabe \t \t ... \t au");
		System.out.println("Verwalten \t \t ... \t v");
		System.out.println("Beliebiges Zeichen um zu Verlsassen");
		
		wahl = scan.next();
		switch(wahl){
		case "m":
			System.out.println("Geben sie ihrer Mannschaft einen Namen:");
			String name = scan.next();
			System.out.println("Wer ist der Kapitaen?");
			String kapitaen = scan.next();
			sql.ausgabeAbteilung();
			System.out.println("Zu welcher Abteilung gehoert sie(abteilungs_ID)?");
			int abteilungID = scan.nextInt();
			System.out.println("Wer trainiert sie?");
			String trainer = scan.next();
			sql.NeueMannschaft(name, kapitaen, abteilungID, trainer);
			break;
		case "s":
			System.out.println("Nachname des Spielers:");
			name = scan.next();
			System.out.println("Vorname:");
			String vname = scan.next();
			System.out.println("PLZ:");
			String plz = scan.next();
			System.out.println("Ort:");
			String ort = scan.next();
			System.out.println("Strasse");
			String strasse = scan.next();
			System.out.println("Geburtsdatum: (YYYY-MM-DD)");
			String gebD = scan.next();
			
			int[] manIDs = new int[9];
			boolean weiter = true;
			while(weiter)
			{
				System.out.println("In welcher Mannschaft spielt der Spieler?");
				sql.ausgabeMannschaft();
				int i = 0;
				manIDs[i] = scan.nextInt();
				i++;
				System.out.println("Spielt der Spieler in einer weiteren Mannschaft?");
				System.out.println("JA! \t ...y");
				System.out.println("NEIN! \t ...n");
				if(scan.next().equals("n"))
				{
					weiter = false;
				}
				
			}
			System.out.println("Seit wann spielt er in dieser/diesen Mannschaft(en)? (YYYY-MM-DD)");
			String seit = scan.next();
			sql.NeuerSpieler(name, vname, plz, ort, strasse, gebD, manIDs, seit);
			break;
		case "a":
			System.out.println("Wie soll die Abteilung heissen?");
			vname = scan.next();
			System.out.println("Wer ist der Leiter?");
			String leiter = scan.next();
			sql.NeueAbteilung(vname, leiter);
			break;
		case "au":
			Ausgabe();
			break;
		case "v":
			Verwalten();
			break;
		default:
			return false;
		}
		
		return true;
	}

	public static void Ausgabe() throws SQLException
	{
		System.out.println("\n\n\nWas wollen sie ausgeben?");
		System.out.println("Alle Mannschaften \t...\tm\nAlle Spieler\t\t...\t s"
				+ "\nAlle Abteilungen \t...\ta");
		wahl = scan.next();
		switch(wahl)
		{
		case "m":
			sql.ausgabeMannschaft();
			break;
		case "s":
			sql.ausgabeSpieler();
			break;
		case "a":
			sql.ausgabeAbteilung();
			break;
		default:
			Menue();
		}
	}

	public static void Verwalten() throws SQLException
	{
		String tab;
		int zeile;
		String idname = " ";
		
		System.out.println("\n\n\nAktion waehlen: \n"
				+ "Eintrag loeschen\t...\tl\n"
				+ "Entrag bearbeiten(noch nicht implementiert)\t...\tb\n");
		wahl = scan.next();
		if(wahl.equals("l"))
		{
			System.out.println("Aus welcher Tabelle soll geloescht werden?\n");
			System.out.println("Mannschaft\t...\tm\n"
					+ "Spieler\t...\ts\n"
					+ "Abteilung\t...\ta");
			tab = scan.next();
			switch(tab){
			case "m":
				sql.ausgabeMannschaft();
				tab = "mannschaft";
				idname = "mannschafts_ID";
				break;
			case "s":
				sql.ausgabeSpieler();
				tab = "spieler";
				idname = "mitgliedsnummer";
				break;
			case "a":
				sql.ausgabeAbteilung();
				tab = "abteilung";
				idname = "abteilungs_ID";
				break;
			default:
				System.out.println("Bitte geben Sie m,s oder a ein!");
				break;
			}
			System.out.println("Welche Zeile soll geloescht werden?(id)");
			zeile = scan.nextInt();
			
			if(sql.Loeschen(tab, zeile, idname))
			{
				System.out.println("Loeschen erfolgreich!");
			}
			
		}
	}

	public static void Datenbankerzeugen() throws SQLException
	{
		sql.createDatabase();
	}
}
