package mainPackage;

import java.sql.*;
import java.util.Arrays;
import java.util.Scanner;
public class Main {
	
	
	static final int numb =13;
	static final int colr = 4;
	static Scanner scan = new Scanner(System.in);
	static int runden = 10000;
	
	static int[] ergebnisse = new int[7];
	
	
	public static void main(String args []) throws Exception
	{
		int[] deck = new int[52];
		
		//Runden festlegen
		//--------------------------------------------------------------------------------------------------------------------------
		System.out.println("Bitte geben sie die Anzahl der Runden an");
		int in;
		//boolean weiter = true;
		
		in = scan.nextInt();
		runden = in;
		//------------------------------------------------------------------------------------------------------------------------
		
		
		for(int i = 0; i < deck.length; i++)
		{
			deck[i] = i;
		}
		
		int[] hand = new int[5];
		
		int ii=0;
		while(ii<=runden)
		{
			hand = newHand(deck);
			
			//pairs
			if((simillarCards(hand)) == 1)
			{
				ergebnisse[0]++;
			}
			
			//three of a kind
			if((simillarCards(hand)) == 2)
			{
				ergebnisse[1]++;
			}
			
			//four of a kind
			if((simillarCards(hand)) == 3)
			{
				ergebnisse[2]++;
			}
			
			//flush
			if((flush(hand)))
			{
				ergebnisse[3]++;
			}
			
			//straight
			if(straight(hand))
			{
				ergebnisse[4]++;
			}
			
			//fullhouse
			if(fullHouse(hand))
			{
				ergebnisse[5]++;
			}
			
			//straightFlush
			if(straigthFlush(hand))
			{
				ergebnisse[6]++;
			}
			
			
			
			
			ii++;
		}
		
		double[] dErgebnisse = getPercentage(ergebnisse);
		//Ausgabe------------------------------------------------------------------------------
		System.out.println("P�rchen:" + dErgebnisse[0]);
		System.out.println("Drillinge:" + dErgebnisse[1]);
		System.out.println("Vierlinge:" + dErgebnisse[2]);
		System.out.println("Flushes:" + dErgebnisse[3]);
		System.out.println("Straights:" + dErgebnisse[4]);
		System.out.println("FullHouses:" + dErgebnisse[5]);
		System.out.println("Straight Flushes:" + dErgebnisse[6]);
		
		//Sqlite insert und Taballe erstellen
		//--------------------------------------------------------------------------------------
				Connection c = null;
			
				try{
					Class.forName("org.sqlite.JDBC");
					c=DriverManager.getConnection("jdbc:sqlite:poker1.db");
					c.setAutoCommit(false);
					
				}
				catch (Exception e)
				{
					System.err.println(e.getClass().getName() + ": " + e.getMessage() );
					System.exit(0);
				}
				
				//-------------------------------------------------------------------
				//TImestamp
				long unixTimestamp = System.currentTimeMillis() / 1000;
				//-------------------------------------------------------------------------------------------
				
		Statement stat = c.createStatement();
		
	    
	    stat = c.createStatement();
	    
	  //  ResultSet rst = stat.executeQuery("drop table if exists results;");
	    
	      String sql = "CREATE TABLE if not exists results " +
	                   " ("
	                   + "id int primary key not null,"
	                   + "time int not null,"
	                  // + "runden int not null"
	                   + " parchen int     NOT NULL," +
	                   " drillinge int NOT NULL, " + 
	                   " vielringe int NOT NULL, " + 
	                   " flushes int not null, " + 
	                   " straights int not null," +
	                   "fullouses int not null," +
	                   "poker int not null)";
	      stat.executeUpdate(sql);
	      
	      PreparedStatement ps = c.prepareStatement("INSERT INTO results (id, time, parchen, drillinge, vielringe, flushes, straights, fullouses, poker)VALUES(?,?,?,?,?,?,?,?,?)");
	      ps.setInt(1,1);
          ps.setInt(2,(int) unixTimestamp);
        //  ps.setInt(3, runden);
          ps.setInt(3,(int) ergebnisse[0] );
          ps.setInt(4, (int) ergebnisse[1]);
          ps.setInt(5, (int) ergebnisse[2]);
          ps.setInt(6, (int) ergebnisse[3]);
          ps.setInt(7, (int) ergebnisse[4]);
          ps.setInt(8, (int) ergebnisse[5]);
          ps.setInt(9, (int) ergebnisse[6]);
          ps.executeUpdate();
          
          
	      
	      stat.close();
	      
	    /*  
	    Statement ps = c.createStatement();
	    */
	 
	    ResultSet rs = stat.executeQuery("select * from results;");
	    
	    System.out.println("\n \n \n");
	    while (rs.next())
	    {
	      System.out.println("ID = " + rs.getInt("id"));
	      System.out.println("Timestamp = " + rs.getInt("time"));
	     // System.out.println("Runden =" + rs.getInt("runden"));
	      System.out.println("Paerchen = " + rs.getInt("parchen"));
	      System.out.println("Drillinge = " + rs.getInt("drillinge"));
	      System.out.println("Vierlinge = " + rs.getInt("vielringe"));
	      System.out.println("Flushes = " + rs.getInt("flushes"));
	      System.out.println("Straights = " + rs.getInt("straights"));
	      System.out.println("Fullhouses = " + rs.getInt("fullouses"));
	      System.out.println("StraightFlus = " + rs.getInt("poker"));
	    }
	    
	   rs.close();
	   c.close();
		//------------------------------------------------------------------------------------------
	}
	
	public static int getCardValue(int card)
	{
		return card % numb;
	}
	
	public static int getCardColor(int card)
	{
		return card % colr;
	}
	
	public static int[] newHand(int[] dec)
	{
		int hand[] = new int[5];
		int deck[] = dec;
		
		
		for(int i = 0; i < 5; i++)
		{
			int poss =  (int) (Math.random() * (deck.length-i));
			hand[i] = deck[poss];
			
			deck[poss] = deck[deck.length - (i+1)];
			
			deck[poss] = deck[deck.length - (i+1)] = hand[i];
		}
		
		return hand;
	}

	
	public static int simillarCards(int hand[])
	{
		int iCounter = 0;
				for( int i = 1; i < hand.length; i++)
		{
			if(getCardValue(hand[i]) == getCardValue(hand[i-1]) ) 
			{
				iCounter++;
			}
		}
				return iCounter;
	}
	

	public static double[] getPercentage(int[] res) {
		int iCounter = 0;
		double[] arr = new double[7];
		for(int i = 0;i < res.length;i++)
		{
			iCounter += res[i];
		}
		for (int i = 0; i < res.length; i++) {
			arr[i] = (res[i] * 100.0) / iCounter;
		}
		return arr;
	}



	public static boolean flush(int[] hand) {
		for (int i = 0; i < hand.length - 1; i++) {
			if (getCardColor(hand[i]) != getCardColor(hand[i + 1])) {
				return false;
			}
		}
		return true;
	}

	public static boolean straight(int[] hand) {
		Arrays.sort(hand);
		for (int i = 1; i < hand.length - 1; i++) {
			if (getCardValue(hand[i]) - getCardValue(hand[i - 1]) != 1) {
				return false;
			}
		}
		return true;
	}

	public static boolean fullHouse(int[] hand) {
		int comp = 0;
		boolean a = false;
		boolean c = false;
		for (int i = 0; i < hand.length - 2; i++) {

			if (getCardValue(hand[i]) == getCardValue(hand[i + 1]) && getCardValue(hand[i + 1]) == getCardValue(hand[i + 2])) {
				a = true;
				comp = getCardValue(hand[i]);
			}

		}
		for (int i = 0; i < hand.length - 1; i++) {
			if (getCardValue(hand[i]) == getCardValue(hand[i + 1]) && getCardValue(hand[i]) != comp) {
				c = true;
			}

		}
		return c && a;
	}

	public static boolean straigthFlush(int[] hand) {
		boolean b = true;
		boolean a = true;
		Arrays.sort(hand);

		for (int i = 1; i < hand.length - 1; i++) {
			if ((getCardValue(hand[i]) - getCardValue(hand[i - 1])) != 1) {
				b = false;
			}
		}
		for (int i = 0; i < hand.length - 1; i++) {
			if (getCardColor(hand[i]) != getCardColor(hand[i + 1])) {
				a = false;
			}
		}
		
		return a&&b;
	}
}
