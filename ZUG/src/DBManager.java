import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DBManager {
	
	String dbName;
	String tbName1;
	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getTbName1() {
		return tbName1;
	}

	public void setTbName1(String tbName1) {
		this.tbName1 = tbName1;
	}

	public String getTbName2() {
		return tbName2;
	}

	public void setTbName2(String tbName2) {
		this.tbName2 = tbName2;
	}

	public String getTbName3() {
		return tbName3;
	}

	public void setTbName3(String tbName3) {
		this.tbName3 = tbName3;
	}

	String tbName2;
	String tbName3;
	String t1c1;
    String t1c2;
	String t1c3;
	String t2c1;
	String t2c2;
	String t3c1;
	String t3c2;
	
	Connection c = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String comm = "";
	
	public DBManager() throws SQLException
	{
		ConfigManager cm = new ConfigManager();
		dbName = cm.getDbName();
		tbName1 = cm.getTbName1();
		tbName2 = cm.getTbName2();
		tbName3 = cm.getTbName3();
		t1c1 = cm.getT1C1();
		t1c2 = cm.getT1C2();
		t1c3 = cm.getT1C3();
		t2c1 = cm.getT2C1();
		t2c2 = cm.getT2C2();
		t3c1 = cm.getT3C1();
		t3c2 = cm.getT3C2();
		OpenConnection();
		System.out.println("First connection to zug.db was successfull!");
		c.close();
		createDatabase();
	}
	
	public void OpenConnection() throws SQLException 
	{
		
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection(("jdbc:sqlite:"+ dbName ));
			c.setAutoCommit(false);
			
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}
		comm ="PRAGMA foreign_keys = 1;";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
	}
//TODO fix foreign keys
	public void createDatabase() throws SQLException
	{
		OpenConnection();
		
		comm = "create table if not exists " + tbName1 + "("
				+ t1c1 + " integer primary key,"
				+ t1c2 + " iteger not null,"
				+ t1c3 + " text not null);";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists " + tbName2 + " ( "
				+ t2c1 + " integer,"
				+ t2c2 + " integer, "
						+ " FOREIGN KEY( " + t2c2 + ") REFERENCES " + tbName1 + "(" + t1c2 + ") , "
						+ " FOREIGN KEY( " + t2c1 + ") REFERENCES " + tbName3 + "(" + t3c1 + ") , "
						+ "Primary Key (" + t2c1 + ", " + t2c2 +")"
								+ ");";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		comm = "create table if not exists " + tbName3 + "("
				+ t3c1 + " integer primary key,"
				+ t3c2 + " integer not null);";
		ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		
	}

	//IF the table has 2 columns with integer, integer
	public void insert(String table, int val1, int val2) throws SQLException
	{
		OpenConnection();
		String cn1;
		String cn2;
		
		if(table.equals(tbName2))
		{
			cn1 = t2c1;
			cn2 = t2c2;
		}else if(table.equals(tbName3)){
			cn1 = t3c1;
			cn2 = t3c2;
		}else
		{
			return;
		}
		
		ps = c.prepareStatement("INSERT INTO " + table +
				"(" + cn1 +"," + cn2 +" )VALUES(?,?)");
	      ps.setInt(1, val1);
	      ps.setInt(2, val2);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      c.close();
	}
	
	//IF the table has 3 columns(integer, integer, text)
	public void insert(String table, int val1, int val2, String val3) throws SQLException
	{
		OpenConnection();
		
		String cn1;
		String cn2;
		String cn3;
		
		if(table.equals(tbName1))
		{
			cn1 = t1c1;
			cn2 = t1c2;
			cn3 = t1c3;
		}else
		{
			return;
		}
		
		PreparedStatement ps = c.prepareStatement("INSERT INTO " + table +
				"(" + cn1 +"," + cn2 +", " + cn3 + ")VALUES(?,?,?)");
	      ps.setInt(1, val1);
	      ps.setInt(2, val2);
	      ps.setString(3, val3);
	      ps.executeUpdate();
	      ps.close();
	      c.commit();
	      c.close();
	}

	public int countPersons() throws SQLException
	{
		int anz = 0;
		OpenConnection();
		comm = ("select count(*) from " + tbName1 + ";");
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		
		while(rs.next())
		{
			anz = rs.getInt(1);
		}
		ps.close();
		rs.close();
		c.close();
		return anz;
	}
	
	public void personsClasses() throws SQLException
	{
		OpenConnection();
		comm = "select " + t1c3 + " , " + t3c2 + " from " + tbName1 
				+ " join " + tbName2 + " using (" + t1c2 + ") join "
				+ tbName3 + " using (" + t3c1 + ");";
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		while(rs.next())
		{
			System.out.println(rs.getString(1) + "\t" + rs.getInt(2));
			
		}
		ps.close();
		rs.close();
		c.close();
	}
	
	public void numberPeopleClasses() throws SQLException
	{
		OpenConnection();
		for (int i = 1; i <= 2; i++) {
			comm = ("select count(*) from " + tbName1 + " join " + tbName2
					+ " using( " + t1c2 + " ) " + " join " + tbName3
					+ " using( " + t3c1 + " ) where " + t3c2 + " =" + i + ";");
			ps = c.prepareStatement(comm);
			rs = ps.executeQuery();
			System.out.println("People in Klass:" + i);
			while(rs.next())
			{
				System.out.println(rs.getInt(1));
			}
			ps.close();
			rs.close();
		}
		c.close();
		
	}
	
	public boolean loeschen(String tab, int zeile, String idName) throws SQLException
	{
		OpenConnection();
		String comm = "DELETE FROM " + tab + " WHERE " + idName +  "=" + zeile;
		PreparedStatement ps = c.prepareStatement(comm);
		ps.executeUpdate();
		c.commit();
		ps.close();
		c.close();
		return true;
		
	}

	public String getT1c1() {
		return t1c1;
	}

	public void setT1c1(String t1c1) {
		this.t1c1 = t1c1;
	}

	public String getT2c1() {
		return t2c1;
	}

	public void setT2c1(String t2c1) {
		this.t2c1 = t2c1;
	}

	public String getT3c1() {
		return t3c1;
	}

	public void setT3c1(String t3c1) {
		this.t3c1 = t3c1;
	}
}
