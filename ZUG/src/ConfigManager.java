import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class ConfigManager {
	
	private String dbName = "zug.db";
	private String tbName1 = "kunde";
	private String tbName2 = "buchung";
	private String tbName3 = "wagon";
	private String t1c1 = "kunden_ID";
	private String t1c2 = "sitzplatz";
	private String t1c3 = "name";
	private String t2c1 = "wagon_ID";
	private String t2c2 = t1c2;
	private String t3c1 = t2c1;
	private String t3c2 = "klasse";
	
	Properties prop = new Properties();
	OutputStream output = null;
	InputStream input = null;
	
	public ConfigManager()
	{
		try {
			output = new FileOutputStream("config.properties");
			
			prop.setProperty("dbName", dbName);
			prop.setProperty("tbName1", tbName1);
			prop.setProperty("tbName2", tbName2);
			prop.setProperty("tbName3", tbName3);
			prop.setProperty("t1c1", t1c1);
			prop.setProperty("t1c2", t1c2);
			prop.setProperty("t1c3", t1c3);
			prop.setProperty("t2c1", t2c1);
			prop.setProperty("t2c2", t2c2);
			prop.setProperty("t3c1", t3c1);
			prop.setProperty("t3c2", t3c2);
			
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally{
			if (output != null){
				try{
					output.close();
				} catch (IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	
	public String getDbName()
	{
		String dbName = "";
		dbName = getPropertyM("dbName");
		
		return dbName;
		}


	private String getPropertyM(String name) {
		
		String key = name;
		
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			name = prop.getProperty(key);
		}catch (IOException ex)
		{
			ex.printStackTrace();
		}finally
		{
			if(input != null){
				try{
					input.close();
				}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		return name;
	}
	
	public String getTbName1()
	{
		String tbName1 = "";
		tbName1 = getPropertyM("tbName1");
		
		return tbName1;
	}

	public String getTbName2()
	{
		String tbName2 = "";
		tbName2 = getPropertyM("tbName2");
		
		return tbName2;
	}
	
	public String getTbName3()
	{
		String tbName3 = "";
		tbName3 = getPropertyM("tbName3");
		return tbName3;
	}
	
	public String getT1C1()
	{
		return (getPropertyM("t1c1"));
	}
	
	public String getT1C2()
	{
		return (getPropertyM("t1c2"));
	}
	
	public String getT1C3()
	{
		return (getPropertyM("t1c3"));
	}
	
	public String getT2C1()
	{
		return (getPropertyM("t2c1"));
	}
	
	public String getT2C2()
	{
		return (getPropertyM("t2c2"));
	}
	
	public String getT3C1()
	{
		return (getPropertyM("t3c1"));
	}
	
	public String getT3C2()
	{
		return (getPropertyM("t3c2"));
	}
	
	

}
